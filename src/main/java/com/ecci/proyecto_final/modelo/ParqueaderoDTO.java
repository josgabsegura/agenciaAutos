/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecci.proyecto_final.modelo;

/**
 *
 * @author Gabo
 */
public class ParqueaderoDTO {

    private String numero;
    private String ubicacion;
    private AgenciaDTO agenciaDTO;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public AgenciaDTO getAgenciaDTO() {
        return agenciaDTO;
    }

    public void setAgenciaDTO(AgenciaDTO agenciaDTO) {
        this.agenciaDTO = agenciaDTO;
    }

}
