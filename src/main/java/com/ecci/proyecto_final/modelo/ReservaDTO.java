/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecci.proyecto_final.modelo;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Gabo
 */
public class ReservaDTO {

    private Integer reservaId;
    private ClienteDTO clienteDTO;
    private VehiculoDTO vehiculoDTO;
    private AgenciaDTO agenciaDTO;
    private Calendar fechaInicio;
    private Calendar fechaFinal;
    private Integer iva;
    private Integer costoTotal;

    public ClienteDTO getClienteDTO() {
        return clienteDTO;
    }

    public void setClienteDTO(ClienteDTO clienteDTO) {
        this.clienteDTO = clienteDTO;
    }

    public VehiculoDTO getVehiculoDTO() {
        return vehiculoDTO;
    }

    public void setVehiculoDTO(VehiculoDTO vehiculoDTO) {
        this.vehiculoDTO = vehiculoDTO;
    }

    public AgenciaDTO getAgenciaDTO() {
        return agenciaDTO;
    }

    public void setAgenciaDTO(AgenciaDTO agenciaDTO) {
        this.agenciaDTO = agenciaDTO;
    }

    public Calendar getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Calendar fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Calendar getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Calendar fechaFinal) {
        this.fechaFinal = fechaFinal;
    }    

    public Integer getIva() {
        return iva;
    }

    public void setIva(Integer iva) {
        this.iva = iva;
    }

    public Integer getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(Integer costoTotal) {
        this.costoTotal = costoTotal;
    }

    public Integer getReservaId() {
        return reservaId;
    }

    public void setReservaId(Integer reservaId) {
        this.reservaId = reservaId;
    }

}
