/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecci.proyecto_final.controlador.conexion;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Gabo
 */
public class ConexionDB {

    private static Connection connetion = null;
    private static final Driver driver = new org.apache.derby.jdbc.ClientDriver();
    private static String dbURL = "jdbc:derby://localhost:1527/agencia";
    private static String usuario = "root";
    private static String contrasena = "root";

    private static void getConnection() {
        try {
            DriverManager.registerDriver(driver);
            connetion = DriverManager.getConnection(dbURL, usuario, contrasena);
        } catch (SQLException ex) {
            Logger.getLogger(ConexionDB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Connection getInstace() {
        if (connetion == null) {
            getConnection();
        }
        return connetion;
    }

    public static void main(String[] args) throws SQLException {
        connetion = ConexionDB.getInstace();
        if (Objects.nonNull(connetion)) {
            JOptionPane.showMessageDialog(null, "Conecto!!");
        }
    }

}
