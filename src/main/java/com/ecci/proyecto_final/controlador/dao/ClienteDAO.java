/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecci.proyecto_final.controlador.dao;

import com.ecci.proyecto_final.controlador.conexion.ConexionDB;
import com.ecci.proyecto_final.modelo.ClienteDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gabo
 */
public class ClienteDAO {

    Connection con = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    List<ClienteDTO> listaClientes = new ArrayList();

    public ClienteDAO() {
        con = ConexionDB.getInstace();
    }

    public List<ClienteDTO> listarAgencias() {
        try {
            pstm = con.prepareStatement("SELECT * FROM ROOT.CLIENTE");
            rs = pstm.executeQuery();
            while (rs.next()) {
                ClienteDTO clienteDTO = new ClienteDTO();
                clienteDTO.setId(rs.getInt("ID"));
                clienteDTO.setNombre(rs.getString("NOMBRE"));
                clienteDTO.setTelefono(rs.getString("TELEFONO"));
                listaClientes.add(clienteDTO);
            }
        } catch (SQLException sqle) {
            System.out.println("Error de Consulta: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());

        } catch (Exception ex) {
            System.out.println("Error en la ejecución:"
                    + " " + ex.getMessage());

        }
        return listaClientes;
    }

    public void registrarCliente(ClienteDTO clienteDTO) {
        try {
            pstm = con.prepareStatement(" INSERT INTO ROOT.CLIENTE (NOMBRE, DIRECCION, TELEFONO) VALUES (?, ?, ?) ");
            pstm.setString(1, clienteDTO.getNombre());
            pstm.setString(2, clienteDTO.getDireccion());
            pstm.setString(3, clienteDTO.getTelefono());
            pstm.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println("Error de Consulta: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());

        } catch (Exception ex) {
            System.out.println("Error en la ejecución:"
                    + " " + ex.getMessage());

        }
    }
}
