/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecci.proyecto_final.controlador.dao;

import com.ecci.proyecto_final.controlador.conexion.ConexionDB;
import com.ecci.proyecto_final.modelo.ReservaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Calendar;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Gabo
 */
public class ReservaDAO {

    Connection con = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;

    public ReservaDAO() {
        con = ConexionDB.getInstace();
    }

    public void almacenarReserva(ReservaDTO reservaDTO) {
        try {
            pstm = con.prepareStatement(" INSERT INTO ROOT.RESERVA (CLIENTE_ID, VEHICULO_ID, AGENCIA_ID, FECHA_INICIO, FECHA_FIN, IVA, COSTO_TOTAL) VALUES (?, ?, ?, ?, ?, ?,?) ");
            pstm.setInt(1, reservaDTO.getClienteDTO().getId());
            pstm.setInt(2, reservaDTO.getVehiculoDTO().getId());
            pstm.setInt(3, reservaDTO.getAgenciaDTO().getId());
            LocalDate localDate = LocalDate.of(reservaDTO.getFechaInicio().get(Calendar.YEAR), reservaDTO.getFechaInicio().get(Calendar.MONTH), reservaDTO.getFechaInicio().get(Calendar.DAY_OF_MONTH));
            LocalDate localDate2 = LocalDate.of(reservaDTO.getFechaFinal().get(Calendar.YEAR), reservaDTO.getFechaFinal().get(Calendar.MONTH), reservaDTO.getFechaFinal().get(Calendar.DAY_OF_MONTH));
            java.sql.Date sqlFechaInicio = java.sql.Date.valueOf(localDate);
            java.sql.Date sqlFechaFin = java.sql.Date.valueOf(localDate2);
            pstm.setDate(4, sqlFechaInicio);
            pstm.setDate(5, sqlFechaFin);
            pstm.setInt(6, reservaDTO.getIva());
            pstm.setInt(7, reservaDTO.getCostoTotal());
            pstm.executeUpdate();
        } catch (SQLException sqle) {
            System.out.println("Error de Consulta: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());

        } catch (Exception ex) {
            System.out.println("Error en la ejecución:"
                    + " " + ex.getMessage());

        }
    }

    public DefaultTableModel mostrarReservas(DefaultTableModel modelo) {
        try {
            pstm = con.prepareStatement(" SELECT r.id,c.NOMBRE,v.MODELO ||'-'||v.PLACA PLACA,r.FECHA_INICIO,r.FECHA_FIN,r.IVA,r.COSTO_TOTAL FROM ROOT.RESERVA r JOIN ROOT.CLIENTE c ON c.ID = r.CLIENTE_ID JOIN ROOT.VEHICULO v ON v.ID = r.VEHICULO_ID ");
            rs = pstm.executeQuery();
            while (rs.next()) {
                String[] insert = new String[7];
                insert[0] = rs.getString("ID");
                insert[1] = rs.getString("NOMBRE");
                insert[2] = rs.getString("PLACA");
                insert[3] = rs.getString("FECHA_INICIO");
                insert[4] = rs.getString("FECHA_FIN");
                insert[5] = rs.getString("IVA");
                insert[6] = rs.getString("COSTO_TOTAL");
                modelo.addRow(insert);
            }
        } catch (SQLException sqle) {
            System.out.println("Error de Consulta: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());

        } catch (Exception ex) {
            System.out.println("Error en la ejecución:"
                    + " " + ex.getMessage());

        }
        return modelo;
    }
}
