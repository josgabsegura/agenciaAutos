/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ecci.proyecto_final.controlador.dao;

import com.ecci.proyecto_final.controlador.conexion.ConexionDB;
import com.ecci.proyecto_final.modelo.AgenciaDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gabo
 */
public class AgenciaDAO {

    Connection con = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    List<AgenciaDTO> listaAgencias = new ArrayList();
    
     public AgenciaDAO() {
        con = ConexionDB.getInstace();
    }

    public List<AgenciaDTO> listarAgencias() {
        try {
            pstm = con.prepareStatement("SELECT * FROM ROOT.AGENCIA");
            rs = pstm.executeQuery();
            while (rs.next()) {
                AgenciaDTO agenciaDTO = new AgenciaDTO();
                agenciaDTO.setId(rs.getInt("ID"));
                agenciaDTO.setNombre(rs.getString("NOMBRE"));
                agenciaDTO.setTelefono(rs.getString("TELEFONO"));
                listaAgencias.add(agenciaDTO);
            }
        } catch (SQLException sqle) {
            System.out.println("Error de Consulta: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());

        } catch (Exception ex) {
            System.out.println("Error en la ejecución:"
                    + " " + ex.getMessage());

        }
        return listaAgencias;
    }
}
