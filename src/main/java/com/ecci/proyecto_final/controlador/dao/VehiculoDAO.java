package com.ecci.proyecto_final.controlador.dao;


import com.ecci.proyecto_final.controlador.conexion.ConexionDB;
import com.ecci.proyecto_final.modelo.VehiculoDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gabo
 */
public class VehiculoDAO {

    Connection con = null;
    PreparedStatement pstm = null;
    ResultSet rs = null;
    List<VehiculoDTO> listaVehiculos = new ArrayList();
    
    public VehiculoDAO() {
        con = ConexionDB.getInstace();
    }
    
    public List<VehiculoDTO> listarVehiculos() {
        try {
            pstm = con.prepareStatement("SELECT * FROM ROOT.VEHICULO");           
            rs = pstm.executeQuery();
            while (rs.next()) {
                VehiculoDTO vehiculoDTO = new VehiculoDTO();
                vehiculoDTO.setId(rs.getInt("ID"));
                vehiculoDTO.setModelo(rs.getString("MODELO"));
                vehiculoDTO.setMarca(rs.getString("MARCA"));
                vehiculoDTO.setValorDia(rs.getInt("VALOR_DIA"));
                vehiculoDTO.setPlaca(rs.getString("PLACA"));                
                vehiculoDTO.setSerie(rs.getString("SERIE"));               
                listaVehiculos.add(vehiculoDTO);
            }
        } catch (SQLException sqle) {
            System.out.println("Error de Consulta: "
                    + sqle.getErrorCode() + " " + sqle.getMessage());

        } catch (Exception ex) {
            System.out.println("Error en la ejecución:"
                    + " " + ex.getMessage());

        }
        return listaVehiculos;
    }
}
